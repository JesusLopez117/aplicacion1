//Hechar un servidor

const http = require("http");
const express = require("express");

const app = express();
const bodyparser = require("body-parser");  //Libreria para trabajar con el metodo get y post

app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public/"));
app.use(bodyparser.urlencoded({extended:true}));

//Declarar un arrays de Objetos

let datos = [{
    matricula: "2020030250",
    nombre: "Alejandro Tirado",
    sexo: "M",
    materias: ["Ingles", "Base de Datos", "Tecnología I"]
},
{
    matricula: "2020030123",
    nombre: "Alejandro Ivan",
    sexo: "M",
    materias: ["Ingles", "Base de Datos", "Tecnología I"]
},
{
    matricula: "2020030845",
    nombre: "Arias Arenas",
    sexo: "M",
    materias: ["Ingles", "Base de Datos", "Tecnología I"]
},
{
    matricula: "2020030674",
    nombre: "Karen Lopez",
    sexo: "F",
    materias: ["Ingles", "Base de Datos", "Tecnología I"]
},
{
    matricula: "2020030232",
    nombre: "Jesús Daniel López Robles",
    sexo: "M",
    materias: ["Ingles", "Base de Datos", "Tecnología I"]
}
]

app.get("/", (req, res) => {
	res.render("index", { titulo: "Mi primera aplicación hecha en Embedded JavaScript", 
    nombre: "Jesús Daniel López Robles", grupo: "TCI 8-3" , listado: datos});
});

//Usado para que entre en cuanto abre la pagína por primera vez y no tiene nada
//El get lo realiza por URl y se usa "query".
//El get realiza la peticion por URL.
app.get("/tabla", (req, res) =>{
    const params = {
        numero: req.query.numero
    }
    res.render('tabla', params);
});

//Usado cuando se recarga la pagina
//El post lo realiza mediante el metodo body.
app.post('/tabla', (req, res) =>{

    const params = {
        numero: req.body.numero
    }
    res.render('tabla', params);  //Renderizar, en este apartado no lleva diagonal
})

//Get para la pagina de cotización
app.get("/cotizacion", (req, res) =>{
    const params = {
        valor: req.query.valor,
        plazo : parseInt(req.query.plazo),
        pInicial : parseInt(req.query.pInicial)
    }

    res.render('cotizacion',params);
})

//Post para la pagina de cotización.
app.post("/cotizacion", (req, res) =>{
    const params = {
        valor: req.body.valor,
        plazo : parseInt(req.body.plazo),
        pInicial : parseInt(req.body.pInicial)
    }

    res.render('cotizacion',params);
})

//La pagína de error va al final de los get/post
app.use((req,res,next) =>{
    res.status(404).sendFile(__dirname + '/public/error.html');
});

const puerto = 3000;
app.listen(puerto, () => {
	console.log("Iniciando puerto");
});